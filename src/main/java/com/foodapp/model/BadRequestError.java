package com.foodapp.model;

import lombok.Data;

@Data
public class BadRequestError {

    private String errorMessage;

    public BadRequestError(IllegalArgumentException e) {
        errorMessage = e.getMessage();
    }
}
