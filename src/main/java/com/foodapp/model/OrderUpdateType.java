package com.foodapp.model;

public enum OrderUpdateType {
    CLIENT_PAYMENT_REQUESTED,
    ORDER_PAID
}
