package com.foodapp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private User client;

    @ManyToOne(cascade = CascadeType.ALL)
    private User server;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private ClientTable clientTable;

    @NotNull
    @NotEmpty
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<OrderedProduct> orderedProducts;

    @Enumerated(EnumType.STRING)
    private PaymentMode paymentMode;

    private Boolean wasPaid;
}
