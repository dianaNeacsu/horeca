package com.foodapp.model;

public enum PaymentMode {
    CASH,
    CARD
}
