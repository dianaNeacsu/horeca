package com.foodapp.model;

import lombok.Data;

@Data
public class OrderUpdateNotification {

    private final Long timestamp;
    private final Order updatedOrder;
    private final OrderUpdateType orderUpdateType;
}
