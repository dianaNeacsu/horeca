package com.foodapp.repository;

import com.foodapp.model.Order;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByWasPaid(Boolean wasPaid);
    
    List<Order> findAllByWasPaidOrWasPaidNull(Boolean wasPaid);
}