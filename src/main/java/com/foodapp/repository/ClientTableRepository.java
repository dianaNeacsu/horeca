package com.foodapp.repository;

import com.foodapp.model.ClientTable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientTableRepository extends JpaRepository<ClientTable, Integer> {

}
