package com.foodapp.service;

import com.foodapp.model.User;
import com.foodapp.model.UserPasswords;
import java.util.Optional;

public interface UserService {

    Optional<User> findUserByEmail(String email);

    void saveUser(User user);

    boolean updateUserPassword(User userEntity, UserPasswords userPasswords);
}
