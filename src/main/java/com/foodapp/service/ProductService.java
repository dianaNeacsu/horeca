package com.foodapp.service;

import com.foodapp.model.Product;
import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> findAllProducts();

    Optional<Product> findProductById(Integer id);

}
