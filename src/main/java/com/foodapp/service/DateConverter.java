package com.foodapp.service;

import com.foodapp.model.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.extern.log4j.Log4j;

@Converter
@Log4j
public class DateConverter implements AttributeConverter<Date, String> {

    @Override
    public String convertToDatabaseColumn(Date dateOfBirth) {
        return new SimpleDateFormat(User.DATE_FORMAT).format(dateOfBirth);
    }

    @Override
    public Date convertToEntityAttribute(String dateOfBirth) {
        try {
            return new SimpleDateFormat(User.DATE_FORMAT).parse(dateOfBirth);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }

}
