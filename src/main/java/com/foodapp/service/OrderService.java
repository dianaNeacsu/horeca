package com.foodapp.service;

import com.foodapp.model.Order;
import com.foodapp.model.OrderUpdateNotification;
import java.util.List;
import java.util.Optional;

public interface OrderService {

    Integer createNewOrder(Order newOrder);

    void updateOrder(Order order);

    Optional<Order> findByIdNormalized(Integer id);

    Optional<Order> findById(Integer id);

    List<Order> getAllCurrentOrders();

    List<Order> getAllPaidOrders();

    OrderUpdateNotification getLatestOrderUpdateOrWait(Long timestamp);
}
