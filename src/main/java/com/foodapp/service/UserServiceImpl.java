package com.foodapp.service;

import com.foodapp.model.Role;
import com.foodapp.model.User;
import com.foodapp.model.UserPasswords;
import com.foodapp.repository.RoleRepository;
import com.foodapp.repository.UserRepository;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    private static final String USER_ROLE = "USER";

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Optional<Role> userRole = roleRepository.findByName(USER_ROLE);
        if (userRole.isPresent()) {
            user.setRoles(new HashSet<>(Collections.singletonList(userRole.get())));
            userRepository.save(user);
        } else {
            throw new IllegalStateException("No role found for " + USER_ROLE);
        }
    }

    @Override
    public boolean updateUserPassword(User userEntity, UserPasswords userPasswords) {
        if (bCryptPasswordEncoder.matches(userPasswords.getOldPassword(), userEntity.getPassword())) {
            userEntity.setPassword(bCryptPasswordEncoder.encode(userPasswords.getNewPassword()));
            userRepository.save(userEntity);
            return true;
        }
        return false;
    }
}
