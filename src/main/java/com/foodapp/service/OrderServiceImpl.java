package com.foodapp.service;

import static java.lang.Boolean.TRUE;
import static org.springframework.util.CollectionUtils.isEmpty;

import com.foodapp.model.ClientTable;
import com.foodapp.model.Order;
import com.foodapp.model.OrderUpdateNotification;
import com.foodapp.model.OrderUpdateType;
import com.foodapp.model.OrderedProduct;
import com.foodapp.model.PaymentMode;
import com.foodapp.model.Product;
import com.foodapp.model.User;
import com.foodapp.repository.ClientTableRepository;
import com.foodapp.repository.OrderRepository;
import com.foodapp.repository.OrderedProductRepository;
import com.foodapp.repository.ProductRepository;
import com.foodapp.repository.UserRepository;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    private static final long POLL_WAIT_MILLIS = 2000L;
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientTableRepository clientTableRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderedProductRepository orderedProductRepository;

    private final NavigableMap<Long, OrderUpdateNotification> updatedOrders = new TreeMap<>();

    @Override
    public Optional<Order> findById(Integer id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            return Optional.empty();
        }
        return Optional.of(order);
    }

    @Override
    public Optional<Order> findByIdNormalized(Integer id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            return Optional.empty();
        }
        normalizeOrder(order);
        return Optional.of(order);
    }

    @Override
    public Integer createNewOrder(Order newOrder) {
        if (newOrder.getId() != null) {
            newOrder.setId(null);
        }
        checkAndSetClientIfValid(newOrder);
        checkAndSetClientTableIfValid(newOrder);
        checkAndSetProductsIfValid(newOrder);

        Order orderEntity = orderRepository.save(newOrder);
        saveAllOrderedProducts(orderEntity);
        return orderEntity.getId();
    }

    @Override
    public void updateOrder(Order order) {
        checkAndSetClientIfValid(order);
        checkAndSetServerIfValid(order);
        checkAndSetClientTableIfValid(order);
        checkAndSetProductsIfValid(order);

        Order oldEntity = orderRepository.findOne(order.getId());
        Boolean oldWasPaid = oldEntity.getWasPaid();
        PaymentMode oldPaymentMode = oldEntity.getPaymentMode();

        saveAllOrderedProducts(order);

        createNotifications(oldWasPaid, oldPaymentMode, order);
    }

    private void createNotifications(Boolean oldWasPaid, PaymentMode oldPaymentMode, Order orderEntity) {
        normalizeOrder(orderEntity);
        createPaymentModeNotification(oldPaymentMode, orderEntity);
        createWasPaidNotification(oldWasPaid, orderEntity);
    }

    private void createPaymentModeNotification(PaymentMode oldPaymentMode, Order updatedEntity) {
        long currentMillis = new Date().getTime();
        if (oldPaymentMode == null && updatedEntity.getPaymentMode() != null) {
            updatedOrders.put(currentMillis,
                    new OrderUpdateNotification(currentMillis, updatedEntity, OrderUpdateType.CLIENT_PAYMENT_REQUESTED));
        }
    }

    private void createWasPaidNotification(Boolean oldWasPaid, Order updatedEntity) {
        long currentMillis = new Date().getTime();
        if (!TRUE.equals(oldWasPaid) && TRUE.equals(updatedEntity.getWasPaid())) {
            updatedOrders.put(currentMillis, new OrderUpdateNotification(currentMillis, updatedEntity, OrderUpdateType.ORDER_PAID));
        }
    }

    @Override
    public List<Order> getAllCurrentOrders() {
        return orderRepository.findAllByWasPaidOrWasPaidNull(false).stream().map(this::normalizeOrder).collect(Collectors.toList());
    }

    @Override
    public List<Order> getAllPaidOrders() {
        return orderRepository.findAllByWasPaid(true).stream().map(this::normalizeOrder).collect(Collectors.toList());
    }

    @Override
    public OrderUpdateNotification getLatestOrderUpdateOrWait(Long updateTimestamp) {
        Entry<Long, OrderUpdateNotification> foundOrderNotification = updatedOrders.ceilingEntry(updateTimestamp + 1);
        while (foundOrderNotification == null) {
            try {
                Thread.sleep(POLL_WAIT_MILLIS);
                foundOrderNotification = updatedOrders.ceilingEntry(updateTimestamp + 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return foundOrderNotification.getValue();
    }

    private void checkAndSetClientIfValid(Order newOrder) {
        if (newOrder.getClient() == null || newOrder.getClient().getEmail() == null) {
            throw new IllegalArgumentException("Invalid order: client user was not provided");
        }
        Optional<User> clientUserOptional = userRepository.findByEmail(newOrder.getClient().getEmail());
        if (clientUserOptional.isPresent()) {
            newOrder.setClient(clientUserOptional.get());
        } else {
            throw new IllegalArgumentException("Invalid order: client user '" + newOrder.getClient().getEmail() + "' does not exist");
        }
    }

    private void checkAndSetServerIfValid(Order newOrder) {
        if (newOrder.getServer() == null || newOrder.getServer().getEmail() == null) {
            throw new IllegalArgumentException("Invalid order: server user was not provided");
        }
        Optional<User> serverUserOptional = userRepository.findByEmail(newOrder.getServer().getEmail());
        if (serverUserOptional.isPresent()) {
            newOrder.setServer(serverUserOptional.get());
        } else {
            throw new IllegalArgumentException("Invalid order: server user '" + newOrder.getServer().getEmail() + "' " + "does not exist");
        }
    }

    private Order normalizeOrder(Order order) {
        if (order.getClient() != null) {
            order.getClient().setPassword(null);
        }
        if (order.getServer() != null) {
            order.getServer().setPassword(null);
        }
        order.getOrderedProducts().forEach(orderedProduct -> orderedProduct.getProduct().setImageHolder(null));
        return order;
    }

    private void checkAndSetClientTableIfValid(Order newOrder) {
        if (newOrder.getClientTable() == null || newOrder.getClientTable().getId() == null) {
            throw new IllegalArgumentException("Invalid order: client table was not provided");
        }
        ClientTable clientTable = clientTableRepository.findOne(newOrder.getClientTable().getId());
        if (clientTable == null) {
            throw new IllegalArgumentException("Invalid order: client table '" + newOrder.getClientTable().getId() + "' does not exist");
        }
        newOrder.setClientTable(clientTable);
    }

    private void checkAndSetProductsIfValid(Order newOrder) {
        if (isEmpty(newOrder.getOrderedProducts())) {
            throw new IllegalArgumentException("Invalid order: no product was ordered");
        }
        for (OrderedProduct orderedProduct : newOrder.getOrderedProducts()) {
            checkAndSetProductIfValid(orderedProduct);
            checkOrderedQuantity(orderedProduct);
            if (orderedProduct.getId() == null || orderedProductRepository.findOne(orderedProduct.getId()) == null) {
                orderedProduct.setOrderTime(new Date());
            }
            orderedProduct.setDeliveryTime(orderedProduct.getDeliveryTime());
        }
    }

    private void checkAndSetProductIfValid(OrderedProduct orderedProduct) {
        if (orderedProduct.getProduct() == null || orderedProduct.getProduct().getId() == null) {
            throw new IllegalArgumentException("Invalid order: ordered product cannot be identified");
        }
        Product product = productRepository.findOne(orderedProduct.getProduct().getId());
        if (product == null) {
            throw new IllegalArgumentException(
                    "Invalid order: ordered product '" + orderedProduct.getProduct().getId() + "' does not exist");
        }
        orderedProduct.setProduct(product);
    }

    private void checkOrderedQuantity(OrderedProduct orderedProduct) {
        if (orderedProduct.getQuantity() == null || orderedProduct.getQuantity() <= 0) {
            throw new IllegalArgumentException(
                    "Invalid order: ordered product '" + orderedProduct.getProduct().getId() + "' has invalid quantity " +
                            orderedProduct.getQuantity());
        }
    }

    private void saveAllOrderedProducts(Order order) {
        Iterable<OrderedProduct> orderedProducts = new CopyOnWriteArrayList<>(order.getOrderedProducts());
        for (OrderedProduct orderedProduct : orderedProducts) {
            if (orderedProduct.getId() != null) {
                OrderedProduct managedOrderProduct = orderedProductRepository.findOne(orderedProduct.getId());
                if (managedOrderProduct != null) {
                    managedOrderProduct.setOrder(order);
                    managedOrderProduct.setQuantity(orderedProduct.getQuantity());
                    managedOrderProduct.setProduct(orderedProduct.getProduct());
                    managedOrderProduct.setOrderTime(orderedProduct.getOrderTime());
                    managedOrderProduct.setDeliveryTime(orderedProduct.getDeliveryTime());
                    managedOrderProduct.setNote(orderedProduct.getNote());
                    orderedProductRepository.save(managedOrderProduct);
                } else {
                    throw new IllegalArgumentException(
                            "Invalid order: ordered product '" + orderedProduct.getId() + "' " + " has invalid identifier " +
                                    orderedProduct.getId());
                }
            } else {
                orderedProduct.setOrder(order);
                orderedProductRepository.save(orderedProduct);
            }
        }
    }
}
