package com.foodapp.controller;

import com.foodapp.model.Product;
import com.foodapp.service.ProductService;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/products")
    public ResponseEntity getAllProducts(@AuthenticationPrincipal Principal principal) {
        List<Product> allProducts = productService.findAllProducts();
        if (allProducts.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(allProducts);
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/products/{id}")
    public ResponseEntity getProductById(@AuthenticationPrincipal Principal principal, @PathVariable("id") Integer id) {
        Optional<Product> product = productService.findProductById(id);
        if (product.isPresent()) {
            return ResponseEntity.ok(product.get());
        }
        return ResponseEntity.noContent().build();
    }
}
