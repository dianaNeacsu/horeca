package com.foodapp.controller;

import com.foodapp.model.User;
import com.foodapp.model.UserPasswords;
import com.foodapp.service.UserService;
import java.security.Principal;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET, value = "/currentUser")
    public ResponseEntity getCurrentUser(@AuthenticationPrincipal Principal principal) {
        Optional<User> optionalUser = userService.findUserByEmail(principal.getName());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setPassword(null);
            return ResponseEntity.ok(user);
        }
        return ResponseEntity.notFound().build();
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.PUT, value = "/currentUser/password")
    public ResponseEntity updatePasswordOfCurrentUser(@AuthenticationPrincipal Principal principal,
            @RequestBody @Valid UserPasswords userPasswords) {
        Optional<User> optionalUser = userService.findUserByEmail(userPasswords.getEmail());
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        if (userService.updateUserPassword(optionalUser.get(), userPasswords)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
