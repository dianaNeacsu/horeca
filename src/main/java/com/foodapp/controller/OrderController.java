package com.foodapp.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import com.foodapp.model.BadRequestError;
import com.foodapp.model.Order;
import com.foodapp.model.OrderUpdateNotification;
import com.foodapp.service.OrderService;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

@Controller
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    private Map<String, DeferredResult> suspendedTradeRequests = new ConcurrentHashMap<String, DeferredResult>();

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity createNewOrder(@RequestBody Order newOrder) {
        try {
            Integer id = orderService.createNewOrder(newOrder);
            return ResponseEntity.status(HttpStatus.OK).location(URI.create(id.toString())).build();
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BadRequestError(e));
        }
    }

    @RequestMapping(method = PUT, value = "/{id}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity updateOrder(@PathVariable("id") Integer id, @RequestBody Order order) {
        Optional<Order> optionalOrder = orderService.findById(id);
        if (!optionalOrder.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        try {
            order.setId(id);
            orderService.updateOrder(order);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BadRequestError(e));
        }
    }

    @RequestMapping(method = GET, value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getOrder(@PathVariable("id") Integer id) {
        Optional<Order> optionalOrder = orderService.findByIdNormalized(id);
        if (!optionalOrder.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(optionalOrder.get());
    }

    @RequestMapping(method = GET, value = "/paid", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllPaidOrders() {
        List<Order> allPaidOrders = orderService.getAllPaidOrders();
        if (isEmpty(allPaidOrders)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(allPaidOrders);
    }

    @RequestMapping(method = GET, value = "/currentOrders", produces = APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity getAllUnpaidOrders() {
        List<Order> allCurrentOrders = orderService.getAllCurrentOrders();
        if (isEmpty(allCurrentOrders)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(allCurrentOrders);
    }

    @RequestMapping(method = GET, value = "/orderNotification/{timestamp}", produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeferredResult<OrderUpdateNotification> listenForOrderNotification(@PathVariable("timestamp") Long timestamp) {
        final DeferredResult<OrderUpdateNotification> result = new DeferredResult<>(null, null);
        OrderUpdateNotification updatedOrder = orderService.getLatestOrderUpdateOrWait(timestamp);
        result.setResult(updatedOrder);
        return result;
    }
}
